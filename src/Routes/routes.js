import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Homepage from '../Components/Home';
import Scroll from '../Components/Scroll/Scroll';
import Login from '../Components/Login/Login';
import Admin from '../Components/Login/Admin'


export default () => (

        <BrowserRouter>
            <div>
                <Switch>
                    <Route exact path="/" component={Homepage} />
                    <Route exact path="/stories" component={Scroll} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/admin" component={Admin} />
                </Switch>
            </div>
        </BrowserRouter>
);

