import React, { Component } from "react";
import { Carousel, Button, Modal } from 'react-bootstrap';

import "./scroll.css";
import red from "../../assets/red.svg";
import gray from "../../assets/gray.png";
import one from "../../assets/ross-sneddon-0MBt0sGU8UA-unsplash.jpg";

class Scroll extends Component {

    state = {
        index: 0,
        setIndex: 0,
        content: [],
        visibility: false,
        chosenLink:''
    };


    handleSelect = (selectedIndex, e) => {
        this.setState({ index: selectedIndex })
    }


    toggleVisibility = (link) => {
        this.setState({
          visibility: !this.state.visibility,
          chosenLink: link
        });
    }


    getSortedPostsData = () => {
        fetch(`https://photobank-api.neukleos.com/page`, {
            method: 'GET',
            headers: {  
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                // 'Authorization': `Bearer ${}`
            },
        })
        .then(response => response.json())
        .then(data => {
            console.log(data.data)
            return this.setState({
                content: data.data,
            });
        });
    }

    componentDidMount = () => {
        this.getSortedPostsData()
    }


    render() {

        const { content } = this.state;

      return (

        <div className="contain">

            <div className="menu-button">
                <a href="/"><img src={red} alt="menu" className="redr" /></a>
            </div>

            <Carousel activeIndex={this.state.index} onSelect={this.handleSelect}>

                <Carousel.Item>
                    <img
                    className="d-block w-100 backdrop-height"
                    src={gray}
                    alt="First slide"
                    />
                    
                    <Carousel.Caption>
                        <h5 className="first-hero-p-text" style={{marginTop:"15%"}}>Content studio</h5>
                        <h3 className="first-hero-large">We craft purpose driven brand stories that move minds, culture and markets</h3>
                        {/* <div className="menu-button">
                            <a href="/"><img src="/red.svg" alt="menu" className="redr" /></a>
                        </div> */}
                    </Carousel.Caption>
                </Carousel.Item>

                {content.map((res) =>
                    <Carousel.Item key={res.id}>
                        <img
                        className="d-block w-100 backdrop-height"
                        src={res.background_image ? `https://photobank-api.neukleos.com/${res.background_image}` : one}
                        alt="First slide"
                        />
                        
                        <Carousel.Caption>
                            <h5 className="hero-p-text leftA" style={{color:"lightgray"}}>{res.logo}</h5>
                            <h3 className="hero-large leftA">{res.title}</h3>
                            <p className="hero-p-text leftA">{res.description}</p>
                            <Button variant="light" className="float-left link-button" onClick={() => this.toggleVisibility(`${res.video_link}`)}>WATCH FILM</Button>{' '}
                        </Carousel.Caption>
                    </Carousel.Item>
                )}

            </Carousel>

            {/* Modal */}
            <Modal show={this.state.visibility} onHide={this.toggleVisibility}>
            <Modal.Header closeButton> </Modal.Header>
            <Modal.Body>
                <div className='row'>
                    <div className='col-sm-12'>
                
                    <div className="section" style={{height: 'auto'}}>
                        <iframe title="brand-video" className="video" src={this.state.chosenLink} frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>

                    </div>
                </div>
            </Modal.Body>
            </Modal>
            
        </div>
    )}
}

export default Scroll;