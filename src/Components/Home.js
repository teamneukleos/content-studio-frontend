import React, { Component } from "react";
// import { getAllImages } from "../../api/image";

import { BrowserRouter as Router } from "react-router-dom";

import "./home.css";
import close from "../assets/close.svg";
import social from "../assets/social.svg";
import neukleos from "../assets/NeukleosC.png";


class Cards extends Component {

  render() {

    return (
      <Router>
        <div className="container">
          <main>
            <div className="">
                <a href="https://www.neukleos.com"><img src={neukleos} style={{width:90, paddingBottom:30}} alt="menu" className="" /></a>
            </div>
            <a href="/stories"><h3 className="hero-large" style={{fontSize:"6rem", lineHeight:"5.5rem", textAlign:"center" }}>stories</h3></a>
            <a href="/"><h3 className="hero-large" style={{fontSize:"6rem", lineHeight:"5.5rem", textAlign:"center" }}>about</h3></a>
          </main>

          <div className="mail-links">
            <div className="content">
              <h5>General Inquiries</h5> 
              <a href="mailto:we@neukleos.com"><p>we@neukleos.com</p></a>
            </div>
          </div>

          <footer>
            <div className="social-links">
              <div className="address-button">
                {/* <a
                href="/"
                target="_blank"
                rel="noopener noreferrer"> */}
                2 Rev Ogunbiyi Street, Ikeja GRA, Lagos, NIGERIA
                {/* </a> */}
              </div>
              <div className="social-button">
                  <a href="/"><img src={social} alt="menu" className="" /></a>
              </div>
              <div className="close-button">
                  <a href="/stories"><img src={close} alt="menu" className="redr" /></a>
              </div>
            </div>
          </footer>
        </div>
      </Router>
    );
  }
}

export default Cards;
