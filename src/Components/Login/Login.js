import React, { Component } from "react";
import "./login.css";



class Login extends Component {

    state = {
        visible: false,
        errorMessage: ""
    }

    onDismiss = () => {
        this.setState({ visible: false });
    }

    handleTextChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    
    handleSubmit = (e) => {
        e.preventDefault();

    }

  render() {
    return (
      <div>
        <div className="login">
          <form className ="login-container" onSubmit={this.handleSubmit}>
            <p>
              <input type="email" placeholder="Email"  name="email"  onChange={this.handleTextChange}  required/>
            </p>
            <p>
              <input type="password" placeholder="Password" name="password"  onChange={this.handleTextChange} required/>
            </p>
            <p>
              <input type="submit" className="redButton" defaultValue="Log in" />
            </p>
          </form>
        </div>
      </div>
    );
  }
}

export default Login;
