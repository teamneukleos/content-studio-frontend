import React , {Component} from "react";
import "./login.css";
import { Carousel, Button, Modal } from 'react-bootstrap';



class Admin extends Component {

    componentDidMount(){
        // if(localStorage.getItem('token') !== null){
        //     this.props.history.push('/');
        // }
    }

    state = {
        visible: false,
        errorMessage: "",
        adminContent: [],
        edit: false,
        create: false,
        delete: false,
    }

    toggleVisibilityE = () => {
      this.setState({
        edit: !this.state.edit,
      });
    }

    toggleVisibilityC = () => {
      this.setState({
        create: !this.state.create,
      });
    }

    toggleVisibilityD = () => {
      this.setState({
        delete: !this.state.delete,
      });
    }


    
    onDismiss = () => {
        this.setState({ visible: false });
    }

    handleTextChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    
    handleSubmit = (e) => {
        e.preventDefault();

    }

  render() {
    return (
      <div>
        <div className="admin">

          <Button variant="light" className="float-left link-button mb-4" style={{width:100}} onClick={() => this.toggleVisibilityC()}>CREATE</Button>{' '} 

          <div className ="admin-container">
              <p>Brand: {}</p>
              <p>Title: </p>
              <p>Description: </p> 
              <p>Video Link: </p>
              <p>Background Image: </p> 
              <Button variant="success" size="sm" className="float-left link-button" onClick={() => this.toggleVisibilityE()}>EDIT</Button>{' '} 
              <Button variant="outline-danger" size="sm" className="float-left link-button ml-2" onClick={() => this.toggleVisibilityD()}>DELETE</Button>
          </div>

        </div>



            {/* Create Modal */}
            <Modal show={this.state.create} onHide={this.toggleVisibilityC}>
            <Modal.Header closeButton> </Modal.Header>
            <Modal.Body>
                <div className="row" style={{background:"white"}}>
                    <form className ="login-container" onSubmit={this.handleSubmit}>
                        <p>
                          <input type="brand" placeholder="Brand"  name="brand"  onChange={this.handleTextChange}  required/>
                        </p>
                        <p>
                          <input type="title" placeholder="Title" name="title"  onChange={this.handleTextChange} required/>
                        </p>
                        <p>
                          <input type="description" placeholder="Description"  name="description"  onChange={this.handleTextChange}  required/>
                        </p>
                        <p>
                          <input type="link" placeholder="VideoLink" name="link"  onChange={this.handleTextChange} required/>
                        </p>
                        <p>
                          <input type="background" className="Background Image" defaultValue="background" />
                        </p>
                        <p>
                          <input type="submit" className="redButton" defaultValue="Log in" />
                        </p>
                    </form>
                </div>
            </Modal.Body>
            </Modal>

            {/* Edit Modal */}
            <Modal show={this.state.edit} onHide={this.toggleVisibilityE}>
            <Modal.Header closeButton> </Modal.Header>
            <Modal.Body>
                <div className='row'>
                  {/* //INSERT EDIT FORM */}
                </div>
            </Modal.Body>
            </Modal>


            {/* Delete Modal */}
            <Modal show={this.state.delete} onHide={this.toggleVisibilityD}>
            <Modal.Header closeButton> </Modal.Header>
            <Modal.Body>
                <div className='row'>
                  {/* //INSERT DELETE PROMPT */}
                </div>
            </Modal.Body>
            </Modal>


      </div>
    );
  }
}

export default Admin;
